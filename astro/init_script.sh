#!/bin/bash


# Función para obtener la IP del servidor
get_server_ip() {
    SERVER_IP=$(curl -s http://checkip.amazonaws.com)
    echo "La IP del servidor es: $SERVER_IP"
}

replace_default_html() {
    sudo mv /var/www/html/index.nginx-debian.html /var/www/html/index.nginx-debian.html.backup  # Respalda el archivo original si es necesario
    sudo cp /usr/local/bin/index.html /var/www/html/index.html
    sudo cp /usr/local/bin/logo.png /var/www/html/logo.png
    sudo rm -r /usr/local/bin/index.html
    sudo rm -r /usr/local/bin/logo.png
    echo "Archivo HTML personalizado reemplazado en Nginx."
}

# Función para instalar Nginx en Ubuntu/Debian
install_nginx_debian() {
    sudo apt update
    sudo apt install -y nginx
    sudo systemctl start nginx
    sudo systemctl enable nginx
    echo "Nginx instalado y arrancado en Ubuntu/Debian."
    replace_default_html
}

# Función para instalar Nginx en CentOS/RHEL
install_nginx_centos() {
    sudo yum update -y
    sudo yum install -y epel-release
    sudo yum install -y nginx
    sudo systemctl start nginx
    sudo systemctl enable nginx
    echo "Nginx instalado y arrancado en CentOS/RHEL."
    replace_default_html
}

# Función para instalar Docker en Ubuntu/Debian
install_docker_debian() {
    sudo apt update
    sudo apt install -y apt-transport-https ca-certificates curl software-properties-common
    curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
    sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/debian bullseye stable"
    sudo apt update
    sudo apt install -y docker-ce
    sudo systemctl start docker
    sudo systemctl enable docker
    echo "Docker instalado y arrancado en Ubuntu/Debian."
}

# Función para instalar Docker en CentOS/RHEL
install_docker_centos() {
    sudo yum update -y
    sudo yum install -y yum-utils
    sudo yum-config-manager --add-repo https://download.docker.com/linux/centos/docker-ce.repo
    sudo yum install -y docker-ce docker-ce-cli containerd.io
    sudo systemctl start docker
    sudo systemctl enable docker
    echo "Docker instalado y arrancado en CentOS/RHEL."
}

# Función para instalar Certbot en Ubuntu/Debian
install_certbot_debian() {
    sudo apt update
    sudo apt install -y certbot python3-certbot-nginx
    echo "Certbot instalado en Ubuntu/Debian."
}

# Función para instalar Certbot en CentOS/RHEL
install_certbot_centos() {
    sudo yum update -y
    sudo yum install -y epel-release
    sudo yum install -y certbot python3-certbot-nginx
    echo "Certbot instalado en CentOS/RHEL."
}

# Detectar la distribución del sistema operativo
if [ -f /etc/os-release ]; then
    # Para sistemas basados en Debian/Ubuntu y Fedora/CentOS
    . /etc/os-release
    OS=$ID
elif [ -f /etc/centos-release ]; then
    # Para sistemas CentOS 6
    OS="centos"
else
    echo "Distribución de Linux no soportada."
    exit 1
fi

# Ejecutar la instalación adecuada según la distribución detectada
case $OS in
    ubuntu|debian)
        install_nginx_debian
        install_docker_debian
        install_certbot_debian
        ;;
    centos|rhel)
        install_nginx_centos
        install_docker_centos
        install_certbot_centos
        ;;
    *)
        echo "Distribución de Linux no soportada."
        exit 1
        ;;
esac


get_server_ip