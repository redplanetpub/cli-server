#!/usr/bin/env python3
import os
import subprocess
from argparse import ArgumentParser

def init():
    script_path = os.path.join(os.path.dirname(__file__), 'init_script.sh')
    subprocess.run(['bash', script_path], check=True)

def main():
    parser = ArgumentParser(description="Astro CLI")
    subparsers = parser.add_subparsers(dest='command')

    parser_init = subparsers.add_parser('init', help='Inicializar proyecto')

    # Aquí añades un argumento que mostrará la versión
    parser.add_argument('--version', action='version', version='astro version 0.1')

    args = parser.parse_args()

    if args.command == 'init':
        init()
    else:
        parser.print_help()

if __name__ == '__main__':
    main()