# CLI-SERVER

ASTRO CLI for servers

## Description

The ASTRO CLI is a powerful tool designed to facilitate various server-related tasks such as database migration, API generation, frontend creation, and server configuration. Built with efficiency in mind, ASTRO CLI streamlines the development process, making it easier for developers to manage and deploy applications.

## Installation

Step-by-step instructions to install the CLI on the server.

```bash
# Download the astro CLI package from the server
wget https://redplanet.mx/astro_package.deb

# Install the CLI package
sudo dpkg -i astro_package.deb

# Grant necessary execution permissions
sudo chmod +x /usr/local/bin/astro