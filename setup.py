from setuptools import setup, find_packages

setup(
    name='astro',
    version='0.1',
    packages=find_packages(),
    entry_points={
        'console_scripts': [
            'astro = astro.cli:main'
        ],
    },
    include_package_data=True,
)