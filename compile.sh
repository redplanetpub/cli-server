#!/bin/bash
mkdir -p astro_package/DEBIAN
mkdir -p astro_package/usr/local/bin
mkdir -p astro_package/usr/local/lib/python3.12.4/dist-packages

# Copiar los archivos necesarios
cp astro/cli.py astro_package/usr/local/bin/astro
cp astro/init_script.sh astro_package/usr/local/bin/init_script.sh
cp astro/index.html astro_package/usr/local/bin/index.html
cp astro/logo.png astro_package/usr/local/bin/logo.png
cp -r astro astro_package/usr/local/lib/python3.12.4/dist-packages/

# Ajustar según la estructura de tu proyecto
cp debian/control astro_package/DEBIAN/

# Crear el archivo de control dentro del paquete .deb
cat <<EOF > astro_package/DEBIAN/control
Package: astro
Version: 0.1
Section: utils
Priority: optional
Architecture: all
Depends: python3, python3-setuptools
Maintainer: Tu Nombre <tu@email.com>
Description: Astro CLI
 CLI para realizar tareas específicas.
EOF

# Empaquetar como .deb
dpkg-deb --build astro_package